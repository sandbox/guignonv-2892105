<?php

/**
 * @file
 * Graphs internal functions.
 */

/**
 * Returns global graph administration form.
 */
function graphs_admin_form($plugin_type) {
  $form = array();

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  return $form;
}

/**
 * Access callback for the entity API.
 */
function graph_sourcer_type_access($op, $type = NULL, $account = NULL) {
  return user_access('administer graph_sourcer', $account);
}

/**
 * Access callback for the entity API.
 */
function graph_renderer_type_access($op, $type = NULL, $account = NULL) {
  return user_access('administer graph_renderer', $account);
}

/**
 * Access callback for the entity API.
 */
function graph_action_type_access($op, $type = NULL, $account = NULL) {
  return user_access('administer graph_action', $account);
}

/**
 * Access callback for the entity API.
 */
function graph_type_access($op, $type = NULL, $account = NULL) {
  return user_access('administer graph entity', $account);
}

/**
 * Access control callback for services.
 */
function graph_sourcer_entity_access($op, $entity) {
  if (user_access('administer graph_sourcer')) {
    return TRUE;
  }

  switch ($op) {
    case 'view':
      if (property_exists($entity, 'status') && $entity->status == 0) {
        return user_access('view unpublished services');
      }
      else {
        return user_access('access graph_sourcer');
      }

    case 'edit':
      return user_access('edit graph_sourcer');

    case 'delete':
      return user_access('delete graph_sourcer');
  }

  return FALSE;
}

/**
 * Access control callback for services.
 */
function graph_renderer_entity_access($op, $entity) {
  if (user_access('administer graph_renderer')) {
    return TRUE;
  }

  switch ($op) {
    case 'view':
      if (property_exists($entity, 'status') && $entity->status == 0) {
        return user_access('view unpublished services');
      }
      else {
        return user_access('access graph_renderer');
      }

    case 'edit':
      return user_access('edit graph_renderer');

    case 'delete':
      return user_access('delete graph_renderer');
  }

  return FALSE;
}

/**
 * Access control callback for services.
 */
function graph_action_entity_access($op, $entity) {
  if (user_access('administer graph_action')) {
    return TRUE;
  }

  switch ($op) {
    case 'view':
      if (property_exists($entity, 'status') && $entity->status == 0) {
        return user_access('view unpublished services');
      }
      else {
        return user_access('access graph_action');
      }

    case 'edit':
      return user_access('edit graph_action');

    case 'delete':
      return user_access('delete graph_action');
  }

  return FALSE;
}

/**
 * Access control callback for services.
 */
function graph_entity_access($op, $entity) {
  if (user_access('administer graph entity')) {
    return TRUE;
  }

  switch ($op) {
    case 'view':
      if (property_exists($entity, 'status') && $entity->status == 0) {
        return user_access('view unpublished services');
      }
      else {
        return user_access('access graph');
      }

    case 'edit':
      return user_access('edit graph');

    case 'delete':
      return user_access('delete graph');
  }

  return FALSE;
}
