<?php

/**
 * @file
 * Graphs Create-Read-Update-Delete functions.
 *
 * @ingroup graphs_graph
 */

/**
 * Shows the graph entity page.
 *
 * @ingroup graphs_graph
 */
function graph_page_view($graph, $view_mode = 'full') {
  return $graph->view($view_mode, NULL, TRUE);
}

/**
 * Returns graph page title.
 *
 * @ingroup graphs_graph
 */
function graph_page_title($graph) {
  return "$graph->label ($graph->id)";
}

/**
 * Returns a graph form.
 *
 * @ingroup graphs_graph
 */
function graph_form($form, &$form_state, $graph) {
  global $user;

  $form['uid'] = array(
    '#value' => $user->uid,
  );
  $form['#id'] = 'graph-form';

  // Store the graph for later.
  $form['#graph'] = $graph;
  $form_state['graph'] = $graph;

  // Label field.
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Graph Entity Name'),
    '#default_value' => isset($graph->label) ? $graph->label : '',
    '#weight' => -5,
    '#required' => TRUE,
    '#access' => user_access('edit graph'),
  );

  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($graph->label) ? $graph->name : '',
    '#maxlength' => 32,
    '#description' => t('A unique machine-readable name of less than 33 characters for this graph. It must only contain lowercase letters, numbers, and underscores.'),
    '#machine_name' => array(
      'source' => array('label'),
      'exists' => 'graph_name_exists',
      'replace_pattern' => '[^a-zA-Z0-9-]+',
      'replace' => '-',
    ),
    '#element_validate' => array('form_validate_machine_name'),
    '#weight' => -4,
    '#access' => user_access('edit graph'),
  );

  // @todo: add radio "auto-display" or "display button" with a customizable label
  // @todo: add height and wrap thoses values into 'dimensions'
  $form['parameters'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
    'dimensions' => array(
      '#type' => 'container',
      'width' => array(
        '#title' => t('Display width'),
        '#type' => 'textfield',
        '#default_value' => isset($graph->dimensions['width']) ? $graph->dimensions['width'] : array(),
        '#description' => t('With of the rendering zone.'),
        '#required' => FALSE,
        '#size' => 30,
      ),
      'height' => array(
        '#title' => t('Display height'),
        '#type' => 'textfield',
        '#default_value' => isset($graph->dimensions['height']) ? $graph->dimensions['height'] : array(),
        '#description' => t('Height of the rendering zone.'),
        '#required' => FALSE,
        '#size' => 30,
      ),
    ),
  );

  $form['additional_settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 99,
  );

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Publishing Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'additional_settings',
    '#weight' => 95,
    '#access' => user_access('edit graph'),
  );

  $form['options']['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Published'),
    '#default_value' => isset($graph->status) ? $graph->status : 0,
  );

  // Buttons.
  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 5,
    '#submit' => array('graph_form_submit'),
  );

  if (!empty($graph->id)) {
    $form['buttons']['delete'] = array(
      '#access' => user_access('delete graph'),
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 15,
      '#submit' => array('graph_form_delete_submit'),
    );
  }

  field_attach_form('graph', $graph, $form, $form_state);

  return $form;
}

/**
 * Machine name callback to make sure it name does not already exist.
 *
 * @ingroup graphs_graph
 */
function graph_name_exists($value) {
  return db_query_range('SELECT 1 FROM {graph} WHERE name = :name', 0, 1, array(':name' => $value))->fetchField();
}

/**
 * Validates graph submission.
 *
 * @ingroup graphs_graph
 */
function graph_form_validate($form, &$form_state) {
  $graph = $form_state['graph'];
  field_attach_form_validate('graph', $graph, $form, $form_state);
}

/**
 * Manages graph submission.
 *
 * @ingroup graphs_graph
 */
function graph_form_submit($form, &$form_state) {
  if (user_access('edit graph')) {
    $form_state['values']['uid'] = $form['uid']['#value'];
  }
  $graph = &$form_state['graph'];

  entity_form_submit_build_entity('graph', $graph, $form, $form_state);
  $graph->save();
  $form_state['redirect'] = 'graph/' . $graph->id;
}

/**
 * Graph edit page.
 *
 * @ingroup graphs_graph
 */
function graph_edit($graph) {
  drupal_set_title(t('Edit Graph Entity: @title', array('@title' => $graph->label)));

  return drupal_get_form('graph_form', $graph);
}

/**
 * Delete confirmation page.
 *
 * @ingroup graphs_graph
 */
function graph_confirm_delete_page($form, &$form_state, $graph) {
  $form = array();

  $form["#graph"] = $graph;
  return confirm_form(
    $form,
    t('Are you sure you want to delete the graph %title?', array('%title' => $graph->label)),
    "graph/$graph->id",
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel'),
    'graph_confirm_delete_page_submit'
  );
}

/**
 * Delete submit callback.
 *
 * @ingroup graphs_graph
 */
function graph_confirm_delete_page_submit($form, &$form_state) {
  $form['#graph']->delete();
  $form_state['redirect'] = "admin/content/graph";
}

/**
 * Submit handler for delete button.
 *
 * @ingroup graphs_graph
 */
function graph_form_delete_submit($form, &$form_state) {
  $form_state['redirect'] = 'graph/' . $form_state['graph']->id . '/delete';
}


/**
 *
 */
function graph_get_graph_data_parameters_ajax($graph) {
  $arguments = drupal_get_query_parameters();

  $node_id = isset($arguments['node_id']) ? $arguments['node_id'] : NULL;

  $depth = isset($arguments['depth']) ? +$arguments['depth'] : 1;

  $rel_types = isset($arguments['rel_types']) ? $arguments['rel_types'] : '';
  if (!is_array($rel_types)) {
    $rel_types = preg_split('/\s*,\s*/', $rel_types);
  }
  $rel_types = array_filter($rel_types);

  $back_rel_types = isset($arguments['back_rel_types']) ? $arguments['back_rel_types'] : '';
  if (!is_array($back_rel_types)) {
    $back_rel_types = preg_split('/\s*,\s*/', $back_rel_types);
  }
  $back_rel_types = array_filter($back_rel_types);

  $rel_filter = isset($arguments['rel_filter']) ? $arguments['rel_filter'] : '';
  if (!is_array($rel_filter)) {
    $rel_filter = preg_split('/\s*,\s*/', $rel_filter);
  }
  $rel_filter = array_filter($rel_filter);

  $rel_exclude = isset($arguments['rel_exclude']) ? $arguments['rel_exclude'] : '';
  if (!is_array($rel_exclude)) {
    $rel_exclude = preg_split('/\s*,\s*/', $rel_exclude);
  }
  $rel_exclude = array_filter($rel_exclude);

  $skip_node_ids = array();
  if (isset($arguments['skip_node_ids'])) {
    foreach (preg_split('/\s*,\s*/', $arguments['skip_node_ids']) as $skip_node_id) {
      $skip_node_ids[$skip_node_id] = TRUE;
    }
  }

  $parameters = array(
    'node_id' => $node_id,
    'depth' => $depth,
    'rel_types' => $rel_types,
    'back_rel_types' => $back_rel_types,
    'rel_filter' => $rel_filter,
    'rel_exclude' => $rel_exclude,
    'skip_node_ids' => $skip_node_ids,
  );

  $sourcer = $graph->getSourcer();

  // Allow sourcer plugin to alter parameters.
  $new_parameters = graphs_call_plugin(
    $sourcer,
    'get_graph_data_parameters_ajax_alter',
    $parameters
  );
  if ($new_parameters) {
    $parameters = $new_parameters;
  }

  $actions = $graph->getActions();
  // Allow action plugins to alter parameters.
  foreach ($actions as $action) {
    $new_parameters = graphs_call_plugin(
      $action,
      'get_graph_data_parameters_ajax_alter',
      $parameters
    );
    if ($new_parameters) {
      $parameters = $new_parameters;
    }
  }

  return $parameters;
}

/**
 *
 */
function graph_get_graph_data_ajax($graph) {
  // Get parameters.
  $parameters = graph_get_graph_data_parameters_ajax($graph);

  $graph_data = $graph->getGraphData($parameters);
  
  return drupal_json_output($graph_data);
}
