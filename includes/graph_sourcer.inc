<?php

/**
 * @file
 * Graph actions Create-Read-Update-Delete functions.
 *
 * @ingroup graphs_sourcer
 */

/**
 * Show the graph data sroucer page.
 *
 * @ingroup graphs_sourcer
 */
function graph_sourcer_page_view($graph_sourcer, $view_mode = 'full') {
  return $graph_sourcer->view($view_mode, NULL, TRUE);
}

/**
 * Returns graph data sourcer page title.
 *
 * @ingroup graphs_sourcer
 */
function graph_sourcer_page_title($graph_sourcer) {
  return $graph_sourcer->label . ' (id:' . $graph_sourcer->id . ')';
}

/**
 * Returns a graph data sourcer form.
 *
 * @ingroup graphs_sourcer
 */
function graph_sourcer_form($form, &$form_state, $graph_sourcer) {
  global $user;

  $form['#attached']['js'][] = array(
    'type' => 'file',
    'data' => drupal_get_path('module', 'graphs') . '/js/graphs.js',
  );

  $form['uid'] = array(
    '#value' => $user->uid,
  );
  $form['#id'] = 'graph_sourcer-form';

  // Store the graph_sourcer for later.
  $form['#graph_sourcer'] = $graph_sourcer;
  $form_state['graph_sourcer'] = $graph_sourcer;

  // Label field.
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Graph Data Sourcer Name'),
    '#default_value' => isset($graph_sourcer->label) ? $graph_sourcer->label : '',
    '#weight' => -5,
    '#required' => TRUE,
    '#access' => user_access('edit graph_sourcer'),
  );

  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($graph_sourcer->label) ? $graph_sourcer->name : '',
    '#maxlength' => 255,
    '#description' => t('A unique name for competitions. It must only contain lowercase letters, numbers and hyphens.'),
    '#machine_name' => array(
      'source' => array('label'),
      'exists' => 'graph_sourcer_name_exists',
      'replace_pattern' => '[^a-z0-9-]+',
      'replace' => '-',
    ),
    '#element_validate' => array('form_validate_machine_name'),
    '#weight' => -10,
    '#access' => user_access('edit graph_sourcer'),
  );

  $sourcer_type_options = array();
  $sourcer_types = entity_load('graph_sourcer_type');
  foreach ($sourcer_types as $sourcer_type) {
    $sourcer_type_options[$sourcer_type->name] = $sourcer_type->label;
  }
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#options' => $sourcer_type_options,
    '#default_value' => isset($graph_sourcer->type) ? $graph_sourcer->type : 1,
    '#description' => t('Data sourcer type.'),
    '#required' => TRUE,
  );

  $form['additional_settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 99,
  );

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Publishing Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'additional_settings',
    '#weight' => 95,
    '#access' => user_access('edit graph_sourcer'),
  );

  $form['options']['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Published'),
    '#default_value' => isset($graph_sourcer->status) ? $graph_sourcer->status : 0,
  );

  // Buttons.
  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 5,
    '#submit' => array('graph_sourcer_form_submit'),
  );

  if (!empty($graph_sourcer->id)) {
    $form['buttons']['delete'] = array(
      '#access' => user_access('delete graph_sourcer'),
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 15,
      '#submit' => array('graph_sourcer_form_delete_submit'),
    );
  }

  field_attach_form('graph_sourcer', $graph_sourcer, $form, $form_state);

  // Call plugins to let them add their features.
  $plugin_info = graphs_plugin_info();

  foreach ($plugin_info['graph_sourcer'] as $sourcer_type => $sourcer_info) {
    $plugin_form = graphs_call_plugin(
      array('plugin_category' => 'graph_sourcer', 'plugin_type' => $sourcer_type),
      'form',
      $graph_sourcer
    );

    // Group fields related to a sourcer type in a same container.
    if (!empty($plugin_form)) {
      $form[$sourcer_type] = array(
        'parameters' => array(
          '#type'        => 'container',
          '#tree'        => TRUE,
          '#weight'      => 0,
        )
        + $plugin_form,
      );
    }
  }

  return $form;
}

/**
 * Machine name callback to make sure it name does not already exist.
 *
 * @ingroup graphs_sourcer
 */
function graph_sourcer_name_exists($value) {
  return db_query_range('SELECT 1 FROM {graph_sourcer} WHERE name = :name', 0, 1, array(':name' => $value))->fetchField();
}

/**
 * Validates graph data sourcer submission.
 *
 * @ingroup graphs_sourcer
 */
function graph_sourcer_form_validate($form, &$form_state) {
  $graph_sourcer = $form_state['graph_sourcer'];
  field_attach_form_validate('graph_sourcer', $graph_sourcer, $form, $form_state);

  // Ignore required values of fields related to other data sourcer types than
  // the one selected.
  // Get a list of all form errors.
  $errors = form_get_errors();
  // Clear Drupal error message queue.
  drupal_get_messages('error');
  // Clear all form errors so we can add all of them back except the ones we
  // want to ignore.
  form_clear_error();
  if (!empty($errors)) {
    foreach ($errors as $field => $message) {
      // The ones we want to keep are related to fields not begining with
      // 'parameters' or that are related to current data sourcer type.
      if ((0 !== strpos($field, 'parameters'))
          || strpos($field, '[' . $form['type']['#value'] . ']')) {
        form_set_error($field, $message);
      }
    }
  }

  // Call plugin to let it validate the form.
  graphs_call_plugin(
    $graph_sourcer,
    'validate',
    $form,
    $form_state
  );

}

/**
 * Manages graph data sourcer submission.
 *
 * @ingroup graphs_sourcer
 */
function graph_sourcer_form_submit($form, &$form_state) {
  if (user_access('edit graph_sourcer')) {
    $form_state['values']['uid'] = $form['uid']['#value'];
  }
  $graph_sourcer = &$form_state['graph_sourcer'];

  entity_form_submit_build_entity(
    'graph_sourcer',
    $graph_sourcer,
    $form,
    $form_state
  );

  // Save associated module.
  $plugin_info = graphs_plugin_info();
  $graph_sourcer->module = $plugin_info['graph_sourcer'][$graph_sourcer->type]['module'];

  // Call plugins to let it process submit.
  graphs_call_plugin(
    $graph_sourcer,
    'submit',
    $form,
    $form_state,
    $graph_sourcer
  );

  $graph_sourcer->save();

  $form_state['redirect'] = 'graph_sourcer/' . $graph_sourcer->id;
}

/**
 * Graph data sourcer edit page.
 *
 * @ingroup graphs_sourcer
 */
function graph_sourcer_edit($graph_sourcer) {
  drupal_set_title(t('Edit Graph Data Sourcer: @title', array('@title' => $graph_sourcer->label)));

  return drupal_get_form('graph_sourcer_form', $graph_sourcer);
}

/**
 * Delete confirmation page.
 *
 * @ingroup graphs_sourcer
 */
function graph_sourcer_confirm_delete_page($form, &$form_state, $graph_sourcer) {
  $form = array();

  $form["#graph_sourcer"] = $graph_sourcer;
  return confirm_form(
    $form,
    t('Are you sure you want to delete the graph_sourcer %title?', array('%title' => $graph_sourcer->label)),
    "graph_sourcer/$graph_sourcer->id",
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel'),
    'graph_sourcer_confirm_delete_page_submit'
  );
}

/**
 * Delete submit callback.
 *
 * @ingroup graphs_sourcer
 */
function graph_sourcer_confirm_delete_page_submit($form, &$form_state) {
  $form['#graph_sourcer']->delete();
  $form_state['redirect'] = "admin/content/graph_sourcer";
}

/**
 * Submit handler for delete button.
 *
 * @ingroup graphs_sourcer
 */
function graph_sourcer_form_delete_submit($form, &$form_state) {
  $form_state['redirect'] = 'graph_sourcer/' . $form_state['graph_sourcer']->id . '/delete';
}
