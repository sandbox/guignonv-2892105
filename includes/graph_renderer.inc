<?php

/**
 * @file
 * Graph actions Create-Read-Update-Delete functions.
 *
 * @ingroup graphs_renderer
 */

/**
 * Show the graph rendering engine page.
 *
 * @ingroup graphs_renderer
 */
function graph_renderer_page_view($graph_renderer, $view_mode = 'full') {
  return $graph_renderer->view($view_mode, NULL, TRUE);
}

/**
 * Returns graph rendering engine page title.
 *
 * @ingroup graphs_renderer
 */
function graph_renderer_page_title($graph_renderer) {
  return "$graph_renderer->label ($graph_renderer->id)";
}

/**
 * Returns a graph rendering engine form.
 *
 * @ingroup graphs_renderer
 */
function graph_renderer_form($form, &$form_state, $graph_renderer) {
  global $user;

  $form['#attached']['js'][] = array(
    'type' => 'file',
    'data' => drupal_get_path('module', 'graphs') . '/js/graphs.js',
  );

  $form['uid'] = array(
    '#value' => $user->uid,
  );
  $form['#id'] = 'graph_renderer-form';

  // Store the graph_renderer for later.
  $form['#graph_renderer'] = $graph_renderer;
  $form_state['graph_renderer'] = $graph_renderer;

  // Label field.
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Graph Rendering Engine Name'),
    '#default_value' => isset($graph_renderer->label) ? $graph_renderer->label : '',
    '#weight' => -5,
    '#required' => TRUE,
    '#access' => user_access('edit graph_renderer'),
  );

  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($graph_renderer->label) ? $graph_renderer->name : '',
    '#maxlength' => 255,
    '#description' => t('A unique name for competitions. It must only contain lowercase letters, numbers and hyphens.'),
    '#machine_name' => array(
      'source' => array('label'),
      'exists' => 'graph_renderer_name_exists',
      'replace_pattern' => '[^a-z0-9-]+',
      'replace' => '-',
    ),
    '#element_validate' => array('form_validate_machine_name'),
    '#weight' => -10,
    '#access' => user_access('edit graph_renderer'),
  );

  $renderer_type_options = array();
  $renderer_types = entity_load('graph_renderer_type');
  foreach ($renderer_types as $renderer_type) {
    $renderer_type_options[$renderer_type->name] = $renderer_type->label;
  }
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#options' => $renderer_type_options,
    '#default_value' => isset($graph_renderer->type) ? $graph_renderer->type : 1,
    '#description' => t('Renderer type.'),
    '#required' => TRUE,
  );

  $form['additional_settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 99,
  );

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Publishing Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'additional_settings',
    '#weight' => 95,
    '#access' => user_access('edit graph_renderer'),
  );

  $form['options']['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Published'),
    '#default_value' => isset($graph_renderer->status) ? $graph_renderer->status : 0,
  );

  // Buttons.
  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 5,
    '#submit' => array('graph_renderer_form_submit'),
  );

  if (!empty($graph_renderer->id)) {
    $form['buttons']['delete'] = array(
      '#access' => user_access('delete graph_renderer'),
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 15,
      '#submit' => array('graph_renderer_form_delete_submit'),
    );
  }

  field_attach_form('graph_renderer', $graph_renderer, $form, $form_state);

  // Call plugins to let them add their features.
  $plugin_info = graphs_plugin_info();

  foreach ($plugin_info['graph_renderer'] as $renderer_type => $renderer_info) {
    $plugin_form = graphs_call_plugin(
      array('plugin_category' => 'graph_renderer', 'plugin_type' => $renderer_type),
      'form',
      $graph_renderer
    );

    // Group fields related to a renderer type in a same container.
    if (!empty($plugin_form)) {
      $form[$renderer_type] = array(
        'parameters' => array(
          '#type'        => 'container',
          '#tree'        => TRUE,
          '#weight'      => 0,
        )
        + $plugin_form,
      );
    }
  }

  return $form;
}

/**
 * Machine name callback to make sure it name does not already exist.
 *
 * @ingroup graphs_renderer
 */
function graph_renderer_name_exists($value) {
  return db_query_range('SELECT 1 FROM {graph_renderer} WHERE name = :name', 0, 1, array(':name' => $value))->fetchField();
}

/**
 * Validates graph rendering engine submission.
 *
 * @ingroup graphs_renderer
 */
function graph_renderer_form_validate($form, &$form_state) {
  $graph_renderer = $form_state['graph_renderer'];
  field_attach_form_validate('graph_renderer', $graph_renderer, $form, $form_state);

  // Ignore required values of fields related to other renderer types than
  // the one selected.
  // Get a list of all form errors.
  $errors = form_get_errors();
  // Clear Drupal error message queue.
  drupal_get_messages('error');
  // Clear all form errors so we can add all of them back except the ones we
  // want to ignore.
  form_clear_error();
  if (!empty($errors)) {
    foreach ($errors as $field => $message) {
      // The ones we want to keep are related to fields not begining with
      // 'parameters' or that are related to current renderer type.
      if ((0 !== strpos($field, 'parameters'))
          || strpos($field, '[' . $form['type']['#value'] . ']')) {
        form_set_error($field, $message);
      }
    }
  }

  // Call plugin to let it validate the form.
  graphs_call_plugin(
    $graph_renderer,
    'validate',
    $form,
    $form_state
  );

}

/**
 * Manages graph rendering engine submission.
 *
 * @ingroup graphs_renderer
 */
function graph_renderer_form_submit($form, &$form_state) {
  if (user_access('edit graph_renderer')) {
    $form_state['values']['uid'] = $form['uid']['#value'];
  }
  $graph_renderer = &$form_state['graph_renderer'];

  entity_form_submit_build_entity(
    'graph_renderer',
    $graph_renderer,
    $form,
    $form_state
  );

  // Save associated module.
  $plugin_info = graphs_plugin_info();
  $graph_renderer->module = $plugin_info['graph_renderer'][$graph_renderer->type]['module'];

  // Call plugins to let it process submit.
  graphs_call_plugin(
    $graph_renderer,
    'submit',
    $form,
    $form_state,
    $graph_renderer
  );

  $graph_renderer->save();
  $form_state['redirect'] = 'graph_renderer/' . $graph_renderer->id;
}

/**
 * Graph rendering engine edit page.
 *
 * @ingroup graphs_renderer
 */
function graph_renderer_edit($graph_renderer) {
  drupal_set_title(t('Edit Graph Rendering Engine: @title', array('@title' => $graph_renderer->label)));

  return drupal_get_form('graph_renderer_form', $graph_renderer);
}

/**
 * Delete confirmation page.
 *
 * @ingroup graphs_renderer
 */
function graph_renderer_confirm_delete_page($form, &$form_state, $graph_renderer) {
  $form = array();

  $form["#graph_renderer"] = $graph_renderer;
  return confirm_form(
    $form,
    t('Are you sure you want to delete the graph_renderer %title?', array('%title' => $graph_renderer->label)),
    "graph_renderer/$graph_renderer->id",
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel'),
    'graph_renderer_confirm_delete_page_submit'
  );
}

/**
 * Delete submit callback.
 *
 * @ingroup graphs_renderer
 */
function graph_renderer_confirm_delete_page_submit($form, &$form_state) {
  $form['#graph_renderer']->delete();
  $form_state['redirect'] = "admin/content/graph_renderer";
}

/**
 * Submit handler for delete button.
 *
 * @ingroup graphs_renderer
 */
function graph_renderer_form_delete_submit($form, &$form_state) {
  $form_state['redirect'] = 'graph_renderer/' . $form_state['graph_renderer']->id . '/delete';
}
