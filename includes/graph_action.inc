<?php

/**
 * @file
 * Graph actions Create-Read-Update-Delete functions.
 *
 * @ingroup graphs_action
 */

/**
 * Show the graph action page.
 *
 * @ingroup graphs_action
 */
function graph_action_page_view($graph_action, $view_mode = 'full') {
  return $graph_action->view($view_mode, NULL, TRUE);
}

/**
 * Returns graph action page title.
 *
 * @ingroup graphs_action
 */
function graph_action_page_title($graph_action) {
  return "$graph_action->label ($graph_action->id)";
}

/**
 * Returns a graph action form.
 *
 * @ingroup graphs_action
 */
function graph_action_form($form, &$form_state, $graph_action) {
  global $user;

  $form['#attached']['js'][] = array(
    'type' => 'file',
    'data' => drupal_get_path('module', 'graphs') . '/js/graphs.js',
  );

  $form['uid'] = array(
    '#value' => $user->uid,
  );
  $form['#id'] = 'graph_action-form';

  // Store the graph_action for later.
  $form['#graph_action'] = $graph_action;
  $form_state['graph_action'] = $graph_action;

  // Label field.
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Graph Action Name'),
    '#default_value' => isset($graph_action->label) ? $graph_action->label : '',
    '#weight' => -5,
    '#required' => TRUE,
    '#access' => user_access('edit graph_action'),
  );

  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($graph_action->label) ? $graph_action->name : '',
    '#maxlength' => 255,
    '#description' => t('A unique name for competitions. It must only contain lowercase letters, numbers and hyphens.'),
    '#machine_name' => array(
      'source' => array('label'),
      'exists' => 'graph_action_name_exists',
      'replace_pattern' => '[^a-z0-9-]+',
      'replace' => '-',
    ),
    '#element_validate' => array('form_validate_machine_name'),
    '#weight' => -10,
    '#access' => user_access('edit graph_action'),
  );

  $action_type_options = array();
  $action_types = entity_load('graph_action_type');
  foreach ($action_types as $action_type) {
    $action_type_options[$action_type->name] = $action_type->label;
  }
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#options' => $action_type_options,
    '#default_value' => isset($graph_action->type) ? $graph_action->type : 1,
    '#description' => t('Action type.'),
    '#required' => TRUE,
  );

  $form['additional_settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 99,
  );

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Publishing Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'additional_settings',
    '#weight' => 95,
    '#access' => user_access('edit graph_action'),
  );

  $form['options']['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Published'),
    '#default_value' => isset($graph_action->status) ? $graph_action->status : 0,
  );

  // Buttons.
  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 5,
    '#submit' => array('graph_action_form_submit'),
  );

  if (!empty($graph_action->id)) {
    $form['buttons']['delete'] = array(
      '#access' => user_access('delete graph_action'),
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 15,
      '#submit' => array('graph_action_form_delete_submit'),
    );
  }

  field_attach_form('graph_action', $graph_action, $form, $form_state);

  // Call plugins to let them add their features.
  $plugin_info = graphs_plugin_info();

  foreach ($plugin_info['graph_action'] as $action_type => $action_info) {
    $plugin_form = graphs_call_plugin(
      array('plugin_category' => 'graph_action', 'plugin_type' => $action_type),
      'form',
      $graph_action
    );

    // Group fields related to an action type in a same container.
    if (!empty($plugin_form)) {
      $form[$action_type] = array(
        'parameters' => array(
          '#type'        => 'container',
          '#tree'        => TRUE,
          '#weight'      => 0,
        )
        + $plugin_form,
      );
    }
  }

  return $form;
}

/**
 * Machine name callback to make sure it name does not already exist.
 *
 * @ingroup graphs_action
 */
function graph_action_name_exists($value) {
  return db_query_range('SELECT 1 FROM {graph_action} WHERE name = :name', 0, 1, array(':name' => $value))->fetchField();
}

/**
 * Validates graph action submission.
 *
 * @ingroup graphs_action
 */
function graph_action_form_validate($form, &$form_state) {
  $graph_action = $form_state['graph_action'];
  field_attach_form_validate('graph_action', $graph_action, $form, $form_state);

  // Ignore required values of fields related to other data action types than
  // the one selected.
  // Get a list of all form errors.
  $errors = form_get_errors();
  // Clear Drupal error message queue.
  drupal_get_messages('error');
  // Clear all form errors so we can add all of them back except the ones we
  // want to ignore.
  form_clear_error();
  if (!empty($errors)) {
    foreach ($errors as $field => $message) {
      // The ones we want to keep are related to fields not begining with
      // 'parameters' or that are related to current data action type.
      if ((0 !== strpos($field, 'parameters'))
          || strpos($field, '[' . $form['type']['#value'] . ']')) {
        form_set_error($field, $message);
      }
    }
  }

  // Call plugin to let it validate the form.
  graphs_call_plugin(
    $graph_action,
    'validate',
    $form,
    $form_state
  );

}

/**
 * Manages graph action submission.
 *
 * @ingroup graphs_action
 */
function graph_action_form_submit($form, &$form_state) {
  if (user_access('edit graph_action')) {
    $form_state['values']['uid'] = $form['uid']['#value'];
  }
  $graph_action = &$form_state['graph_action'];

  entity_form_submit_build_entity(
    'graph_action',
    $graph_action,
    $form,
    $form_state
  );

  // Save associated module.
  $plugin_info = graphs_plugin_info();
  $graph_action->module = $plugin_info['graph_action'][$graph_action->type]['module'];

  // Call plugins to let it process submit.
  graphs_call_plugin(
    $graph_action,
    'submit',
    $form,
    $form_state,
    $graph_action
  );

  $graph_action->save();

  $form_state['redirect'] = 'graph_action/' . $graph_action->id;
}

/**
 * Graph action edit page.
 *
 * @ingroup graphs_action
 */
function graph_action_edit($graph_action) {
  drupal_set_title(t('Edit Graph Action: @title', array('@title' => $graph_action->label)));

  return drupal_get_form('graph_action_form', $graph_action);
}

/**
 * Delete confirmation page.
 *
 * @ingroup graphs_action
 */
function graph_action_confirm_delete_page($form, &$form_state, $graph_action) {
  $form = array();

  $form["#graph_action"] = $graph_action;
  return confirm_form(
    $form,
    t('Are you sure you want to delete the graph_action %title?', array('%title' => $graph_action->label)),
    "graph_action/$graph_action->id",
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel'),
    'graph_action_confirm_delete_page_submit'
  );
}

/**
 * Delete submit callback.
 *
 * @ingroup graphs_action
 */
function graph_action_confirm_delete_page_submit($form, &$form_state) {
  $form['#graph_action']->delete();
  $form_state['redirect'] = "admin/content/graph_action";
}

/**
 * Submit handler for delete button.
 *
 * @ingroup graphs_action
 */
function graph_action_form_delete_submit($form, &$form_state) {
  $form_state['redirect'] = 'graph_action/' . $form_state['graph_action']->id . '/delete';
}
