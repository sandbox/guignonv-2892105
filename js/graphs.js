/**
 * @file
 * Graphs Javascript library.
 */

(function ($) {

// You could add additional behaviors here.
Drupal.behaviors.graphs = {
  attach: function (context, settings) {

    // Initializes graph form behavior.
    $('#graph_sourcer-form .form-item-type select,\
       #graph_renderer-form .form-item-type select,\
       #graph_action-form .form-item-type select')
      .once()
      .on('change', function () {
          $('#graph_sourcer-form .form-wrapper,\
             #graph_renderer-form .form-wrapper,\
             #graph_action-form .form-wrapper').hide();
          $('#graph_sourcer-form .graph-datasource-type-'
            + $(this).val()
            + ', #graph_renderer-form .graph-renderer-type-'
            + $(this).val()
            + ', #graph_action-form .graph-action-type-'
            + $(this).val()
          ).show();
      })
      .trigger("change");

    // Make sure the graphs setting structure has been initialized correctly.
    if (!Drupal.settings.graphs) {
      Drupal.settings.graphs = {
        'graphs': [],
        'sourcers': [],
        'renderers': [],
        'actions': []
      };
    }
    else {
      if (!Drupal.settings.graphs.graphs) {
        Drupal.settings.graphs.graphs = [];
      }
      if (!Drupal.settings.graphs.sourcers) {
        Drupal.settings.graphs.sourcers = [];
      }
      if (!Drupal.settings.graphs.renderers) {
        Drupal.settings.graphs.renderers = [];
      }
      if (!Drupal.settings.graphs.actions) {
        Drupal.settings.graphs.actions = [];
      }
    }

    // Initializes sourcers.
    $.each(Drupal.settings.graphs.sourcers, function (sourcer_id, sourcer_settings) {
      if (Drupal.behaviors[sourcer_settings.module].init) {
        Drupal.behaviors[sourcer_settings.module].init(sourcer_id);
      }
    });

    // Initializes renderers.
    $.each(Drupal.settings.graphs.renderers, function (renderer_id, renderer_settings) {
      if (Drupal.behaviors[renderer_settings.module].init) {
        Drupal.behaviors[renderer_settings.module].init(renderer_id);
      }
    });

    // Initializes actions.
    $.each(Drupal.settings.graphs.actions, function (action_id, action_settings) {
      if (Drupal.behaviors[action_settings.module].init) {
        Drupal.behaviors[action_settings.module].init(action_id);
      }
    });

    // Render graphs.
    $.each(Drupal.settings.graphs.graphs, function (runtime_id, graph) {
      var renderer = Drupal.settings.graphs.renderers[graph.renderer];
      if (Drupal.behaviors[renderer.module].render) {
        Drupal.behaviors[renderer.module].render(runtime_id, graph.renderer);
      }
      else {
        console.log('Graphs: no JS render function for plugin ' + renderer.module);
      }
    });

  },

  detach: function (context, settings) { },

  /**
   * Fetch parameters to use for a GetGraph call.
   *
   * Fetch parameters to use for a GetGraph call and invoke sourcer plugin
   * implementation of prepareGetGraphParameters if available.
   */
  prepareGetGraphParameters: function (sourcer_id, parameters = {}) {

    var sourcer = Drupal.settings.graphs.sourcers[sourcer_id];

    // Check if sourcer was set.
    if (undefined == parameters.sourcer_id) {
      parameters.sourcer_id = sourcer_id;
    }

    // Check if a depth was set.
    if (undefined == parameters.depth) {
      parameters.depth = sourcer.default_depth;
    }

    if (undefined == parameters['skip_node_ids']) {
      skip_node_ids = {};
    }

    if (Drupal.behaviors[sourcer.module].prepareGetGraphParameters) {
      Drupal.behaviors[sourcer.module].prepareGetGraphParameters(parameters);
    }

    return parameters;
  },

  /**
   * Provides a subgraph for the given graph and sourcer.
   *
   * Provides a subgraph for the given graph and sourcer. Also updates current
   * sourcer graph and nodes hash.
   *
   * @param integer runtime_id
   *   a graph runtime id (not the DB id!).
   * @param integer sourcer_id
   *   a sourcer id.
   * @param object parameters
   *   a parameter object. Supported keys:
   *   - node_id string: a node identifier or null to select graph root;
   *   - depth int: the max depth of linked nodes from given node (default 1);
   *   - rel_types array: relationship between the linked nodes and current node.
   *     (default set to "null" which means "any" type of relationship).
   *     The relationship should be read as: "node_id" "rel_types"
   *     "<returned node ids>".
   *   - skip_node_ids object: a hash of linked node id (hash keys) to ignore.
   */
  getGraph: function (runtime_id, sourcer_id, parameters = {}) {
    Drupal.behaviors.graphs.prepareGetGraphParameters(sourcer_id, parameters);
    var sourcer = Drupal.settings.graphs.sourcers[sourcer_id];
    var graph = Drupal.settings.graphs.graphs[runtime_id];
    if (0 >= parameters.depth) {
      // No depth, stop here.
      // Call callback if one.
      if (parameters.callback) {
        parameters.callback({
          'nodes': [],
          'links': []
        });
      }
    }
    else if (undefined == parameters.node_id) {
      // No node specified, return the whole graph.
      if (parameters.callback) {
        parameters.callback(sourcer.graph);
      }
    }
    else {
      // A node has been specified.
      // Remove callback from parameters in order to avoid having it called
      // before/outside the success event if "parameters" values are evaluated.
      var parameters_callback = parameters.callback;
      parameters.callback = null;

      var success_function = function (subgraph) {
        // Combine with the rest of the current graph.
        if (subgraph && subgraph.nodes && subgraph.links) {
          var new_nodes = {};
          // Only add new nodes.
          subgraph.nodes.forEach(function (node) {
            if (!sourcer.node_index[node.id]) {
              // Store nodes in the hash lookup.
              sourcer.node_index[node.id] = node;
              sourcer.graph.nodes.push(node);
              new_nodes[node.id] = true;
            }
          });
          // Only add links related to new nodes.
          subgraph.links.forEach(function (link) {
            if (new_nodes[link.source] || new_nodes[link.target]) {
              if (!sourcer.link_index[link.source]) {
                sourcer.link_index[link.source] = {};
              }
              if (!sourcer.link_index[link.source][link.target]) {
                sourcer.link_index[link.source][link.target] = {};
              }
              sourcer.link_index[link.source][link.target][link.relationship] = link;
              sourcer.graph.links.push(link);
            }
          });
        }

        // Perform actions.
        if (graph.actions && graph.actions.length) {
          graph.actions.forEach(function (action_id) {
            var action_settings = Drupal.settings.graphs.actions[action_id];
            if (Drupal.behaviors[action_settings.module].act) {
              Drupal.behaviors[action_settings.module].act(action_settings, 'getGraph', subgraph);
            }
          });
        }

        // Call renderer callback.
        if (parameters_callback) {
          parameters_callback(subgraph);
        }
      };

      // Check how to get graph: through plugin Javascript or Ajax call (plugin
      // PHP code in Drupal module).
      if (sourcer.getGraph) {
        var subgraph = sourcer.getGraph(runtime_id, sourcer_id, parameters);
        success_function(subgraph);
      }
      else if (Drupal.behaviors[sourcer.module].getGraph) {
        var subgraph = Drupal.behaviors[sourcer.module].getGraph(
          runtime_id,
          sourcer_id,
          parameters
        );
        success_function(subgraph);
      }
      else {
        $.ajax({
          url: Drupal.settings.basePath + 'graph/' + graph.id + '/get-graph',
          data: parameters,
          success: success_function,
          dataType: 'json'
        })
        .fail(function (jqXHR, textStatus) {
          console.log("Failed to get sub-nodes: " + textStatus + jqXHR);
          console.log("URL: " + Drupal.settings.basePath + 'graph/' + graph.id + '/get-graph?' + $.param(parameters));
        });
      }
    }
  }

};

}(jQuery));
