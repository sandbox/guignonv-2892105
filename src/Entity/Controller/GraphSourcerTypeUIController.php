<?php

namespace Drupal\graphs\Entity\Controller;

/**
 * Graph data sourcer type UI controller.
 *
 * @ingroup graphs_sourcer
 */
class GraphSourcerTypeUIController extends \EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage Graph Data Sourcer, including fields.';
    return $items;
  }

}
