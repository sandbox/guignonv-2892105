<?php

namespace Drupal\graphs\Entity\Controller;

/**
 * Graph rendering engine type UI controller.
 *
 * @ingroup graphs_renderer
 */
class GraphRendererTypeUIController extends \EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage Graph Rendering Engine, including fields.';
    return $items;
  }

}
