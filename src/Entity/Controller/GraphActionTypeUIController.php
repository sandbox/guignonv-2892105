<?php

namespace Drupal\graphs\Entity\Controller;

/**
 * Graph action UI controller.
 *
 * @ingroup graphs_renderer
 */
class GraphActionTypeUIController extends \EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage Graph Action, including fields.';
    return $items;
  }

}
