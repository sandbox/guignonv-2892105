<?php

namespace Drupal\graphs\Entity;

/**
 * Graph data sourcer entity.
 *
 * Contains graph data sourcer instance settings.
 *
 * A Graph data sourcer job is to provide a graph JSON structure of the
 * following form:
 * @code
 * {
 *   "nodes": [ // An array of all the nodes, all having the same structure.
 *     // A node structure:
 *     {
 *       "id": "<a unique node identifier which can be numeric or text>"
 *     },
 *     ...
 *   ],
 *   "links": [ // An array of relationship definitions between nodes as below.
 *     {
 *       "source": "<a subject node identifier (id)>",
 *       "target": "<an object node identifier (id)>"
 *     },
 *     ...
 *   ]
 * }
 * @endcode
 * Note: it is NOT forbidden to have more than one or 2 links between 2 same
 * nodes, which may or may not also have different directions. The renderer will
 * deal with it and may discard links if it does not support multiple links
 * between 2 same nodes.
 *
 * Optional node fields:
 * - "class": (text) an array of space-separated CSS class to apply to the node.
 *   It can be used to give a type to the node (ie. "root", "inner",
 *   "leaf",...);
 * - "name": (text) a human-readable label for the node; If not provided, a
 *   renderer may use the id with transformation (capitalize first letter of a
 *   text id or prepend a numeric id with "node " for instance);
 * - "size": (numeric) the size of the node. It can be use to change the size of
 *   the graphic element (disc for instance) that represents the node;
 * - "weight": (numeric) this value can be used by the renderer to sort the
 *   nodes;
 *
 * Optional link fields:
 * - "class": (text) an array of space-separated CSS class to apply to the link.
 *   It can be used to give a type to the link (ie. "weak", "strong", "main",
 *   "cross", ...);
 * - "length": can be used to specify the link length;
 * - "name": (text) a human-readable label for the link. Parent to child link
 *   names are usualy displayed on the left or on top of a link while child
 *   to parent link names are displayed on the right or at the bottom;
 * - "relationship": the type of relationship between the source and the target.
 *   The relationship should be read as "source" "relationship" "target". For
 *   instance "node 1" "parent_of" "node 2". Common relationships are
 *   "parent_of", "child_of", "is_a", "part_of", "derives_from", "instance_of"
 *   just to give a few. If not specified, the default relationship assumed is
 *   "child_of".
 *
 * Example tree:
 * @code
 *   Alpha
 *   /   \Subdivision
 * Beta   \
 *     1st/\
 *   Delta  Epsilon
 * @endcode
 * Corresponding JSON data provided by the sourcer:
 * @code
 * {
 *   "nodes": [
 *     {
 *       "id": "alpha",
 *       "name": "Alpha"
 *     },
 *     {
 *       "id": "beta",
 *       "name": "Beta"
 *     },
 *     {
 *       "id": "gamma",
 *     },
 *     {
 *       "id": "delta",
 *       "name": "Delta"
 *     },
 *     {
 *       "id": "epsilon",
 *       "name": "Epsilon"
 *     }
 *   ],
 *   "links": [
 *     {
 *       "source": "alpha",
 *       "target": "beta",
 *       "relationship": "parent_of"
 *     },
 *     {
 *       "source": "alpha",
 *       "target": "gamma",
 *       "relationship": "parent_of"
 *     },
 *     {
 *       "source": "gamma",
 *       "target": "delta",
 *       "relationship": "parent_of",
 *       "name": "1st"
 *     },
 *     {
 *       "source": "gamma",
 *       "target": "epsilon",
 *       "relationship": "parent_of",
 *     },
 *     {
 *       "source": "beta",
 *       "target": "alpha",
 *       "relationship": "child_of"
 *     },
 *     {
 *       "source": "gamma",
 *       "target": "alpha",
 *       "relationship": "child_of",
 *       "name": "Subdivision"
 *     },
 *     {
 *       "source": "delta",
 *       "target": "gamma",
 *       "relationship": "child_of"
 *     },
 *     {
 *       "source": "epsilon",
 *       "target": "gamma",
 *       "relationship": "child_of"
 *     }
 *   ]
 * }
 * @endcode
 * Javascript getNodes method details
 * ----------------------------------
 * This function is called by the renderer to get complementary nodes from the
 * sourcer.
 *
 * Parameters:
 * - node_id string: a node identifier from which the linked (sub)nodes should
 *   be returned. If not specified, graph root is used.
 * - depth int: maximal depth of the linked (sub)nodes to return. 1 means
 *   directly linked nodes only, 2 means directly linked nodes plus their direct
 *   neighbors. And so on.
 * - rel_types array: relationship types between the given node and linked nodes
 *   that should be taken into account. Linked nodes with only other
 *   relationships will not be returned. If not specified, all relationship are
 *   taken into account.
 * - back_rel_types array: same behavior as rel_types except it consider
 *   backward relationships. rel_types only considers relationships between
 *   the given node and linked nodes but not relationships between linked nodes
 *   and the given node. That is what rel_types is for as relationships are
 *   directed.
 * - skip_node_ids array: contains a list of node identifiers (as hash keys) to
 *   not go through to retrieve their sub-nodes. However, those nodes can be
 *   returned as children of other nodes. This is used to prevent infinite
 *   cycling when fetching sub-graphs.
 *
 * @ingroup graphs_sourcer
 */
class GraphSourcer extends \Entity {

  /**
   * Graph data sourcer entity constructor.
   */
  public function __construct(array $values, $entityType) {
    parent::__construct($values, $entityType);
    if (!isset($this->type)) {
      $this->type = 'graph_sourcer';
    }
  }

  /**
   * Returns a graph data sourcer entity URI.
   */
  public function uri() {
    return [
      'path' => 'graph_sourcer/' . $this->id,
    ];
  }

  /**
   * Prepare graph rendered element.
   *
   * Attaches javascript functions to the rendered element to perform the
   * related functions when displayed.
   */
  public function render($runtime_id) {
    $render_array = array(
      '#attached' => array(
        'js' => array(
          array(
            'type' => 'file',
            'data' => drupal_get_path('module', 'graphs') . '/js/graphs.js',
          ),
        ),
      ),
    );

    $render_array[$this->type] = graphs_call_plugin(
      $this,
      'render',
      $this
    );

    return $render_array;
  }

  // @todo: add a function to provide an example of data.

}
