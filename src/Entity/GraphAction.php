<?php

namespace Drupal\graphs\Entity;

/**
 * Graph action entity.
 *
 * Contains graph action instance settings.
 *
 * @ingroup graphs_action
 */
class GraphAction extends \Entity {

  /**
   * Graph entity constructor.
   */
  public function __construct(array $values, $entityType) {
    parent::__construct($values, $entityType);
    if (!isset($this->type)) {
      $this->type = 'graph_action';
    }
  }

  /**
   * Returns a graph action entity URI.
   */
  public function uri() {
    return [
      'path' => 'graph_action/' . $this->id,
    ];
  }

  /**
   * Prepare graph rendered element.
   *
   * Attaches javascript functions to the rendered element to perform the
   * related functions when displayed.
   */
  public function render($runtime_id) {

    // Creates a block for the rendering.
    $render_array = array(
      $this->type . '_' . $this->id => graphs_call_plugin(
        $this,
        'render',
        $this
      )
    );

    return $render_array;
  }

}
