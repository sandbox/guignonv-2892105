<?php

namespace Drupal\graphs\Entity;

/**
 * Graph entity.
 *
 * Contains graph instance settings which includes a reference to a data sourcer
 * and a reference to a rendering engine and optional references to graph
 * actions.
 *
 * @ingroup graphs_graph
 */
class Graph extends \Entity {

  protected static $uniqueId = 1;

  protected $runtimeId;

  /**
   * Graph entity constructor.
   */
  public function __construct(array $values, $entityType) {
    parent::__construct($values, $entityType);
    if (!isset($this->type)) {
      $this->type = 'graph';
    }

    $this->runtimeId  = self::$uniqueId++;
  }

  /**
   * Returns a graph entity URI.
   */
  public function uri() {
    return array(
      'path' => 'graph/' . $this->id,
    );
  }

  /**
   * Returns a graph renderable array.
   *
   * Prepares the JSON data that will hold grpahs module settings. The data
   * structure is the following:
   * @code
   * {
   *   'graphs': { // Contains graphs module global settings.
   *     // ...
   *     'graphs' { // Contains the list of graph instances keyed by id.
   *       '123': { // Settings for graph instance "123".
   *         'sourcer': '456', // Identifier of the sourcer used by this graph.
   *         'renderer': '789', // Identifier of the renderer used by this graph.
   *         'actions': [101], // An array of action identifiers used by this graph.
   *         'width': '800', // Width of the graph.
   *         'height': '600' // Height of the graph.
   *       }
   *       // ...
   *     },
   *     'sourcers' { // Contains the list of sourcer instances keyed by id.
   *       '456': {
   *         // Settings for sourcer instance "456".
   *         // The graph and nodes keys are updated during runtime to reflect
   *         // current graph.
   *         'graph': {
   *           'nodes': [<node_data>, <node_data>, ...],
   *           'links': [<link_data>]
   *         },
   *         'nodes': {
   *           <node_id>: <node_data>,
   *           <node_id>: <node_data>,
   *           ...
   *         },
   *         // Function to use to fetch nodes linked to a given node.
   *         'getNodes': function (parameters) {...}
   *       },
   *       // ...
   *     },
   *     'renderers' { // Contains the list of renderer instances keyed by id.
   *       '789': {
   *         // Settings for renderer instance "789".
   *         // Function to use to render the graph.
   *         'render': function (graph_index) {...}
   *       },
   *       // ...
   *     },
   *     'actions' { // Contains the list of action instances keyed by id.
   *       '101': {
   *         // Settings for action instance "101".
   *       },
   *       // ...
   *     }
   *   }
   * }
   * @endcode
   *
   * Sourcer and renderer settings are added by the sourcer and renderer modules
   * called through the soucer and renderer hook_render implementation.
   *
   * @see graphs_plugin_info
   * @see hook_graphs_plugin_info
   */
  public function render() {
    $sourcer = $this->getSourcer();
    $renderer = $this->getRenderer();
    $actions = $this->getActions();

    $render_array = array(
      'sourcer' => array(),
      'renderer' => array(),
      'actions' => array(),
    );
    $render_array['sourcer'] = $sourcer ? $sourcer->render($this->runtimeId) : array();
    $render_array['renderer'] = $renderer ? $renderer->render($this->runtimeId) : array();

    $all_action_settings = array();

    foreach ($actions as $action_id => $action) {
      $render_array['actions'][$action_id] = $action->render($this->runtimeId);
      if (isset($render_array['actions'][$action_id][$action->type . '_' . $action_id]['#settings'])) {
        $all_action_settings[$action_id] = $render_array['actions'][$action_id][$action->type . '_' . $action_id]['#settings'];
        $all_action_settings[$action_id] += array(
          'type' => $action->type,
          'module' => $action->module,
        );
      }
    }

    // Allow action plugins to alter nodes.
    $sourcer_settings = array();
    if (isset($render_array['sourcer'][$sourcer->type]['#settings'])) {
      $sourcer_settings = $render_array['sourcer'][$sourcer->type]['#settings'];

      if (isset($render_array['sourcer'][$sourcer->type]['#settings']['graph'])) {
        $graph_data = $render_array['sourcer'][$sourcer->type]['#settings']['graph'];
        $parameters = array();
        foreach ($actions as $action) {
          $new_graph = graphs_call_plugin(
            $action,
            'get_graph_data_alter_return',
            $action,
            $graph_data,
            $parameters
          );
          if ($new_graph) {
            $graph_data = $new_graph;
          }
        }
        $sourcer_settings['graph'] = $graph_data;
        $sourcer_settings['node_index'] = graph_extract_nodes($graph_data);
        $sourcer_settings['link_index'] = graph_extract_links($graph_data);
      }
    }

    // Make sure default fields are initialized.
    $sourcer_settings += array(
      'type' => $sourcer->type,
      'module' => $sourcer->module,
      'node_index' => (object) array(),
      'link_index' => (object) array(),
      'graph' => (object) array(),
    );

    if (isset($render_array['renderer'][$renderer->type]['#settings'])) {
      $renderer_settings = $render_array['renderer'][$renderer->type]['#settings'];
      $renderer_settings += array(
        'type' => $renderer->type,
        'module' => $renderer->module,
      );
    }
    else {
      $renderer_settings = NULL;
    }

    $js_settings = drupal_add_js(
      array(
        'graphs' => array(
          'graphs' => array(
            $this->runtimeId => array(
              'id' => $this->id,
              'sourcer' => $sourcer->id,
              'renderer' => $renderer->id,
              'actions' => array_keys($actions),
              'width' => intval($this->dimensions['width']),
              'height' => intval($this->dimensions['height']),
            ),
          ),
          'sourcers' => array(
            $sourcer->id => $sourcer_settings,
          ),
          'renderers' => array(
            $renderer->id => $renderer_settings,
          ),
          'actions' => $all_action_settings,
        ),
      ),
      array('type' => 'setting')
    );

    return $render_array;
  }

  /**
   * Returns the graph data sourcer object associated with this graph instance.
   */
  public function getSourcer($index = 0) {
    if (!$this->graph_sourcers
        || count($this->graph_sourcers[LANGUAGE_NONE]) <= $index) {
      return FALSE;
    }

    $sourcer_id = $this->graph_sourcers[LANGUAGE_NONE][$index]['target_id'];
    $sourcer = graph_sourcer_load($sourcer_id);

    return $sourcer;
  }

  /**
   * Returns the graph data rendering engine associated with this graph.
   */
  public function getRenderer($index = 0) {
    if (!$this->graph_renderers
        || count($this->graph_renderers[LANGUAGE_NONE]) <= $index) {
      return FALSE;
    }

    $renderer_id = $this->graph_renderers[LANGUAGE_NONE][$index]['target_id'];
    $renderer = graph_renderer_load($renderer_id);

    return $renderer;
  }

  /**
   * Returns the graph actions associated with this graph.
   */
  public function getActions() {
    if (!$this->graph_actions) {
      return array();
    }

    $action_ids = array_map(
      function ($n) { return $n['target_id']; },
      $this->graph_actions[LANGUAGE_NONE]
    );

    $actions = graph_action_load_multiple($action_ids);
    return $actions;
  }

  /**
   *
   */
  public function getGraphData($parameters = array()) {
  
    // Get nodes from sourcer plugin.
    $sourcer = $this->getSourcer();
    $graph_data = graphs_call_plugin(
      $sourcer,
      'get_graph_data',
      $parameters
    );
  
    $actions = $this->getActions();
    // Allow action plugins to alter nodes.
    foreach ($actions as $action) {
      $new_graph = graphs_call_plugin(
        $action,
        'get_graph_data_alter_return',
        $action,
        $graph_data,
        $parameters
      );
      if ($new_graph) {
        $graph_data = $new_graph;
      }
    }
  
    return $graph_data;
  }
}
