<?php

namespace Drupal\graphs\Entity;

/**
 * Graph rendering engine entity.
 *
 * Contains graph renderer instance settings.
 *
 * @ingroup graphs_renderer
 */
class GraphRenderer extends \Entity {

  /**
   * Graph rendering engine entity constructor.
   */
  public function __construct(array $values, $entityType) {
    parent::__construct($values, $entityType);
    if (!isset($this->type)) {
      $this->type = 'graph_renderer';
    }
  }

  /**
   * Returns a graph rendering engine entity URI.
   */
  public function uri() {
    return [
      'path' => 'graph_renderer/' . $this->id,
    ];
  }

  // @todo: add a function to provide an example of rendering.

  /**
   * Prepare graph rendered element.
   *
   * Attaches javascript functions to the rendered element to perform the
   * related functions when displayed.
   */
  public function render($runtime_id) {

    // Creates a block for the rendering.
    $render_array = array(
      'graph_render_block' => array(
        '#type' => 'markup',
        '#markup' => '<div id="graphs_' . $runtime_id . "\"></div>\n",
      ),
    );

    // Call plugin to let it add its features.
    $render_array[$this->type] = graphs_call_plugin(
      $this,
      'render',
      $this
    );

    return $render_array;
  }

}
