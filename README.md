Graphs Extension Module
=======================

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------
This module provides a framework to load and displays graph data with an
interactive user interface through plugins.


REQUIREMENTS
------------

This module requires the following:

 * Entity API
 * Entity Reference
 * X Autoload (dynamic library loading in the Drupal-8-style PSR-4)
 * jQuery >= 1.7 (use jQuery update module)
 * Graph plugins for data loading ("sourcers"), data rendering ("renderers") and
   actions (optional).


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Enable the module in "Admin menu > Site building > Modules" (/admin/modules).


CONFIGURATION
-------------

In order to use  a graph, you need to instanciate at least one graph data
loader, at least one graph data rendering engine and a graph entity that uses
those. Then you can display your graph as a page or a block, in a view area
(header/footer) or place it using a token.


MAINTAINERS
-----------

Current maintainer:

 * Valentin Guignon (vguignon) - https://www.drupal.org/user/423148
