<?php

/**
 * @file
 * Graphs API.
 */

/**
 * Returns the settings of registered plugins.
 *
 * @param bool $reset
 *   If set, reloads the plugin settings and update cache.
 *
 * @return array
 *   The structure of the returned array is describbed below.
 *
 * @code
 *   array(
 *     <plugin_category> => array(
 *       '<sourcer_type_name>' => array(
 *         'module' => '<module_name>',
 *         'version' => '<version_string>',
 *         'compatibility' => array(
 *           'sourcers' => array(
 *             // See compatibility note.
 *             'default' => FALSE,
 *           ),
 *           'renderers' => array(
 *             // See compatibility note.
 *             'default' => TRUE,
 *             '<renderer1_type_name>' => '<version_string>',
 *             '<renderer2_type_name>' => '<version_string>',
 *           ),
 *           'actions' => array(
 *             // See compatibility note.
 *             'default' => TRUE,
 *           ),
 *         ),
 *       ),
 *     ),
 *   );
 * @endcode
 *
 * plugin category can be one of 'graph_sourcer', 'graph_renderer',
 * 'graph_action'.
 *
 * Compatibility note: when a type is not in the list, then the "default" type
 * ('default' key) should be used to set default behavior. If a false value is
 * set, the type is not compatible. If a version string is set, the type is
 * compatible for the given version(s) (supports prefixes '>', '>=', '<',
 * '<=', '!=' for version ranges). Multiple versions can be specified using
 * space or comma as separator and interpreted as "AND" while the double pipe
 * separator will be interpreted as "OR".
 * Compatibility can be adjusted by other modules using
 * hook_graphs_plugin_info_alter so if module A does not know about module B
 * but B does, B can alter A compatibility list to add itself (B) there.
 *
 * @see hook_graphs_plugin_info
 * @see hook_graphs_plugin_info_alter
 */
function graphs_plugin_info($reset = FALSE) {
  static $plugin_info;

  // Check if info has already been gathered and if not, load it.
  if ($reset || !isset($plugin_info)) {
    if (!$reset
          && ($cache = cache_get('GRAPHS_PLUGIN_INFO'))
          && !empty($cache->data)) {
      $plugin_info = $cache->data;
    }
    else {
      $plugin_info = array(
        'graph_sourcer' => array(),
        'graph_renderer' => array(),
        'graph_action' => array(),
      );
      // Call plugins to let them add their features.
      foreach (module_implements('graphs_plugin_info') as $module) {
        $plugin_features = module_invoke($module, 'graphs_plugin_info');
        if (!empty($plugin_features)) {
          foreach ($plugin_info as $type => $settings) {
            if (isset($plugin_features[$type])) {
              $plugin_info[$type] += $plugin_features[$type];
            }
          }
        }
      }
    }
  }

  return $plugin_info;
}

/**
 * Helper function that calls a plugin method.
 *
 * @param mixed $plugin
 *   Machine name of a plugin or a plugin instance.
 * @param string $method
 *   Name of the method to call.
 *
 * @return mixed
 *   The value returned by the plugin method.
 */
function graphs_call_plugin($plugin, $method) {
  $plugin_info = graphs_plugin_info();
  if (is_object($plugin)) {
    $plugin_type = $plugin->type;
    $plugin_category = $plugin->entityType();
  }
  else {
    $plugin_category = $plugin['plugin_category'];
    $plugin_type = $plugin['plugin_type'];
  }

  if (isset($plugin_info[$plugin_category][$plugin_type])
      && $plugin_info[$plugin_category][$plugin_type]['module']) {

    $parameters = func_get_args();
    $parameters[0] = $plugin_info[$plugin_category][$plugin_type]['module'];
    $parameters[1] = 'graphs_' . $parameters[1];
    return call_user_func_array('module_invoke', $parameters);
  }
}

/**
 *
 */
function graphs_install_type($category, $type, $label, $weight = 0) {

  $type = entity_create($category . '_type', array(
    'name' => $type,
    'label' => st($label),
    'weight' => $weight,
  ));

  $type->save();
}

/**
 *
 */
function graphs_uninstall_type($category, $type) {

  // Remove graphs references to $type.
  $ids = db_select($category, 'et')
    ->fields('et')
    ->condition('type', $type)
    ->execute()
    ->fetchAllAssoc('id');

  foreach ($ids as $id => $value) {
    // @todo:
    // $graph = db_update('graphs')
    //   ->fields(array('graph_renderers' => NULL))
    // ->condition('graph_renderers', $id)
    // ->execute();
  }

  // Remove fields attached to $type.
  field_attach_delete_bundle($category, $type);
}

/**
 * Menu argument loader; Load a profile type by string.
 *
 * @param string $type
 *   The machine-readable name of a profile type to load.
 *
 * @return array
 *   A profile type array or FALSE if $type does not exist.
 */
function graph_sourcer_type_load($type) {
  return graph_sourcer_get_types($type);
}

/**
 * Gets an array of all test entity types, keyed by the name.
 *
 * @param string $name
 *   If set, the type with the given name is returned.
 */
function graph_sourcer_get_types($name = NULL) {
  $types = entity_load_multiple_by_name('graph_sourcer_type', isset($name) ? array($name) : FALSE);
  return isset($name) ? reset($types) : $types;
}

/**
 * Loads multiple entities based on certain conditions.
 *
 * @param array $ids
 *   An array of entity IDs.
 * @param array $conditions
 *   An array of conditions to match against the {entity} table.
 * @param bool $reset
 *   A boolean indicating that the internal cache should be reset.
 *
 * @return array
 *   An array of test entity objects, indexed by id.
 */
function graph_sourcer_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('graph_sourcer', $ids, $conditions, $reset);
}

/**
 * Loads a graph_sourcer entity.
 */
function graph_sourcer_load($id = NULL, $reset = FALSE) {
  $ids = (isset($id) ? array($id) : array());
  $graph_sourcer = graph_sourcer_load_multiple($ids, $reset);
  return $graph_sourcer ? reset($graph_sourcer) : FALSE;
}

/**
 * Deletes multiple test entities.
 *
 * @param array $ids
 *   An array of test entity IDs.
 */
function graph_sourcer_delete_multiple(array $ids) {
  entity_get_controller('graph_sourcer')->delete($ids);
}

/**
 * Menu argument loader; Load a profile type by string.
 *
 * @param string $type
 *   The machine-readable name of a profile type to load.
 *
 * @return array
 *   A profile type array or FALSE if $type does not exist.
 */
function graph_renderer_type_load($type) {
  return graph_renderer_get_types($type);
}

/**
 * Gets an array of all test entity types, keyed by the name.
 *
 * @param string $name
 *   If set, the type with the given name is returned.
 */
function graph_renderer_get_types($name = NULL) {
  $types = entity_load_multiple_by_name('graph_renderer_type', isset($name) ? array($name) : FALSE);
  return isset($name) ? reset($types) : $types;
}

/**
 * Loads multiple entities based on certain conditions.
 *
 * @param array $ids
 *   An array of entity IDs.
 * @param array $conditions
 *   An array of conditions to match against the {entity} table.
 * @param bool $reset
 *   A boolean indicating that the internal cache should be reset.
 *
 * @return array
 *   An array of test entity objects, indexed by id.
 */
function graph_renderer_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('graph_renderer', $ids, $conditions, $reset);
}

/**
 * Loads a graph_renderer entity.
 */
function graph_renderer_load($id = NULL, $reset = FALSE) {
  $ids = (isset($id) ? array($id) : array());
  $graph_renderer = graph_renderer_load_multiple($ids, $reset);
  return $graph_renderer ? reset($graph_renderer) : FALSE;
}

/**
 * Deletes multiple test entities.
 *
 * @param array $ids
 *   An array of test entity IDs.
 */
function graph_renderer_delete_multiple(array $ids) {
  entity_get_controller('graph_renderer')->delete($ids);
}

/**
 * Menu argument loader; Load a profile type by string.
 *
 * @param string $type
 *   The machine-readable name of a profile type to load.
 *
 * @return array
 *   A profile type array or FALSE if $type does not exist.
 */
function graph_action_type_load($type) {
  return graph_action_get_types($type);
}

/**
 * Gets an array of all test entity types, keyed by the name.
 *
 * @param string $name
 *   If set, the type with the given name is returned.
 */
function graph_action_get_types($name = NULL) {
  $types = entity_load_multiple_by_name('graph_action_type', isset($name) ? array($name) : FALSE);
  return isset($name) ? reset($types) : $types;
}

/**
 * Loads multiple entities based on certain conditions.
 *
 * @param array $ids
 *   An array of entity IDs.
 * @param array $conditions
 *   An array of conditions to match against the {entity} table.
 * @param bool $reset
 *   A boolean indicating that the internal cache should be reset.
 *
 * @return array
 *   An array of test entity objects, indexed by id.
 */
function graph_action_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('graph_action', $ids, $conditions, $reset);
}

/**
 * Loads a graph_action entity.
 */
function graph_action_load($id = NULL, $reset = FALSE) {
  $ids = (isset($id) ? array($id) : array());
  $graph_action = graph_action_load_multiple($ids, $reset);
  return $graph_action ? reset($graph_action) : FALSE;
}

/**
 * Deletes multiple test entities.
 *
 * @param array $ids
 *   An array of test entity IDs.
 */
function graph_action_delete_multiple(array $ids) {
  entity_get_controller('graph_action')->delete($ids);
}

/**
 * Loads multiple entities based on certain conditions.
 *
 * @param array $ids
 *   An array of entity IDs.
 * @param array $conditions
 *   An array of conditions to match against the {entity} table.
 * @param bool $reset
 *   A boolean indicating that the internal cache should be reset.
 *
 * @return array
 *   An array of test entity objects, indexed by id.
 */
function graph_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('graph', $ids, $conditions, $reset);
}

/**
 * Loads a graph entity.
 */
function graph_load($id = NULL, $reset = FALSE) {
  $ids = (isset($id) ? array($id) : array());
  $graph = graph_load_multiple($ids, $reset);
  return $graph ? reset($graph) : FALSE;
}

/**
 * Deletes multiple test entities.
 *
 * @param array $ids
 *   An array of test entity IDs.
 */
function graph_delete_multiple(array $ids) {
  entity_get_controller('graph')->delete($ids);
}

/**
 *
 */
function graph_extract_nodes($graph_data, &$nodes = array()) {
  foreach ($graph_data['nodes'] as $node) {
    $nodes[$node['id']] = $node;
  }
  return $nodes;
}

/**
 *
 */
function graph_extract_links($graph_data, &$links = array()) {
  foreach ($graph_data['links'] as $link) {
    $links[$link['source']][$link['target']][$link['relationship']] = $link;
  }
  return $links;
}
